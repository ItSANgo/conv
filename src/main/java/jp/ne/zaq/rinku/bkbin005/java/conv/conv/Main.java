/**
 *
 */
package jp.ne.zaq.rinku.bkbin005.java.conv.conv;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * @author Mitsutoshi NAKANO bkbin005@rinku.zaq.ne.jp
 *
 */
public class Main {
  private static final String FROM_CODE = "from-code";
  private static final String TO_CODE = "to-code";
  private static final String DEFAULT_ENCODING = System
      .getProperty("file.encoding");

  /**
   * @param args
   * @throws ParseException
   * @throws IOException
   * @throws FileNotFoundException
   * @throws UnsupportedEncodingException
   */
  public static void main(String[] args) throws ParseException,
      UnsupportedEncodingException, FileNotFoundException, IOException {

    Options options = createOptions();
    CommandLine cmd = parseOptions(options, args);
    String inputEncoding = cmd.getOptionValue(FROM_CODE);
    if (inputEncoding == null) {
      inputEncoding = DEFAULT_ENCODING;
    }
    String outputEncoding = cmd.getOptionValue(TO_CODE);
    if (outputEncoding == null) {
      outputEncoding = DEFAULT_ENCODING;
    }
    (new Converter()).convert(inputEncoding, outputEncoding, cmd.getArgs());
  }

  static CommandLine parseOptions(Options options, String[] args)
      throws ParseException {
    return (new DefaultParser()).parse(options, args);
  }

  static Options createOptions() {
    Options options = new Options();
    options.addOption(new Option("f", FROM_CODE, true, "input encoding"));
    options.addOption(new Option("t", TO_CODE, true, "output encoding"));
    return options;
  }

}
