/**
 *
 */
package jp.ne.zaq.rinku.bkbin005.java.conv.conv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

/**
 * @author Mitsutoshi NAKANO bkbin005@rinku.zaq.ne.jp
 *
 */
public class Converter {

  public void convert(String inputEncoding, String outputEncoding,
      String[] args)
      throws UnsupportedEncodingException, FileNotFoundException, IOException {
    PrintStream out = new PrintStream(System.out, true, outputEncoding);
    for (String arg : args) {
      convertAFile(inputEncoding, arg, out);
    }
  }

  void convertAFile(String inputEncoding, String filename, PrintStream out)
      throws UnsupportedEncodingException, FileNotFoundException, IOException {
    File file = new File(filename);
    try (BufferedReader br = new BufferedReader(
        new InputStreamReader(new FileInputStream(file), inputEncoding))) {
      out.print(br.readLine());
    }
  }

}
